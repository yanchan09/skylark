import { expect, test } from "vitest";
import { JID } from "./index";

const LONG_STRING = "a".repeat(1024);

test("Empty JIDs aren't valid", () => {
  expect(() => new JID("")).toThrow();
});

test("Empty localpart isn't valid", () => {
  expect(() => new JID("@example.com")).toThrow();
});

test("Empty resourcepart isn't valid", () => {
  expect(() => new JID("local@example.com/")).toThrow();
});

test("Length limits are respected", () => {
  expect(() => new JID(LONG_STRING)).toThrow();
  expect(() => new JID(`${LONG_STRING}@example.com`)).toThrow();
  expect(() => new JID(`local@example.com/${LONG_STRING}`)).toThrow();
});

test("Valid JIDs get parsed correctly", () => {
  expect(new JID("juliet@example.com/foo@bar")).toMatchObject({
    domain: "example.com",
    local: "juliet",
    resource: "foo@bar",
  });
  expect(new JID("a.example.com/b@example.com")).toMatchObject({
    domain: "a.example.com",
    local: null,
    resource: "b@example.com",
  });
});

test("toBareString works", () => {
  expect(new JID("juliet@example.com/foo@bar").toBareString()).toBe(
    "juliet@example.com"
  );
  expect(new JID("juliet@example.com").toBareString()).toBe(
    "juliet@example.com"
  );
});

test("toString works", () => {
  const testCases = [
    "juliet@example.com/foo@bar",
    "juliet@example.com",
    "xmpp.example.com",
  ];
  for (const test of testCases) {
    expect(new JID(test).toString()).toBe(test);
  }
});
