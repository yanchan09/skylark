import { JID } from "@skylark/jid";

export enum PresenceType {
  Error = "error",
  Probe = "probe",
  Subscribe = "subscribe",
  Subscribed = "subscribed",
  Unavailable = "unavailable",
  Unsubscribe = "unsubscribe",
  Unsubscribed = "unsubscribed",
}

export enum PresenceShow {
  Away = "away",
  Chat = "chat",
  DoNotDisturb = "dnd",
  ExtendedAway = "xa",
}

export class PresenceStanza {
  from?: JID;
  to?: JID;

  type?: PresenceType;
  show?: PresenceShow;
  status?: string;

  priority = 0;

  static fromDOM(el: Element): PresenceStanza {
    if (el.namespaceURI !== "jabber:client" || el.localName !== "presence") {
      throw new Error("Invalid root element");
    }

    const obj = new PresenceStanza();

    const fromAttr = el.getAttribute("from");
    if (fromAttr !== null) {
      obj.from = new JID(fromAttr);
    }

    const toAttr = el.getAttribute("to");
    if (toAttr !== null) {
      obj.to = new JID(toAttr);
    }

    const typeAttr = el.getAttribute("type");
    if (typeAttr !== null) {
      obj.type = typeAttr as PresenceType;
    }

    for (const child of el.children) {
      if (
        child.namespaceURI === "jabber:client" &&
        child.localName === "show"
      ) {
        obj.show = child.textContent as PresenceShow;
      } else if (
        child.namespaceURI === "jabber:client" &&
        child.localName === "status"
      ) {
        obj.status = child.textContent ?? "";
      }
      if (
        child.namespaceURI === "jabber:client" &&
        child.localName === "priority"
      ) {
        obj.priority = Number(child.textContent);
      }
    }

    return obj;
  }
}

export interface EntityIdentity {
  lang?: string;
  category?: string;
  name?: string;
  type?: string;
}

export interface XFormField {
  name: string;
  values: string[];
}

export interface XFormData {
  formType: string;
  fields: XFormField[];
}

function attrValueOrUndefined(el: Element, attr: string): string | undefined {
  const value = el.getAttribute(attr);
  return value !== null ? value : undefined;
}

export class EntityCapabilities {
  identities: EntityIdentity[] = [];
  features: string[] = [];
  forms: XFormData[] = [];

  static fromDOM(el: Element): EntityCapabilities {
    if (
      el.namespaceURI !== "http://jabber.org/protocol/disco#info" ||
      el.localName !== "query"
    ) {
      throw new Error("Invalid root element");
    }

    const obj = new EntityCapabilities();

    for (const child of el.children) {
      if (
        child.namespaceURI === "http://jabber.org/protocol/disco#info" &&
        child.localName === "identity"
      ) {
        obj.identities.push({
          lang: attrValueOrUndefined(child, "xml:lang"),
          category: attrValueOrUndefined(child, "category"),
          name: attrValueOrUndefined(child, "name"),
          type: attrValueOrUndefined(child, "type"),
        });
      } else if (
        child.namespaceURI === "http://jabber.org/protocol/disco#info" &&
        child.localName === "feature"
      ) {
        const value = child.getAttribute("var");
        if (value !== null) {
          obj.features.push(value);
        }
      }
    }

    return obj;
  }
}
