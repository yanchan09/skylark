import sha1 from "crypto-js/sha1";
import encBase64 from "crypto-js/enc-base64";
import { SkylarkJSX } from "..";
import {
  ComponentKey,
  ComponentRegistry,
  IQ_HANDLER,
  PRESENCE_EXTENSION,
} from "../plumbing/registry";
import { IBaseModule } from "../plumbing/module";

export const ENTITY_FEATURE = Symbol("ENTITY_FEATURE") as ComponentKey<string>;

export interface ClientIdentity {
  nodeUri: string;
  clientName: string;
}

interface CapsIdentity {
  category: string;
  type: string;
  lang?: string;
  name?: string;
}

interface CapsInfo {
  identities: CapsIdentity[];
  features: string[];
}

function compareStrings(a: string, b: string): number {
  if (a < b) {
    return -1;
  } else if (a > b) {
    return 1;
  } else {
    return 0;
  }
}

function calculateCapsHash(caps: CapsInfo) {
  const identitiesSorted = caps.identities.slice();
  identitiesSorted.sort((a, b) => {
    if (a.category != b.category) {
      return compareStrings(a.category, b.category);
    }
    if (a.type != b.type) {
      return compareStrings(a.type, b.type);
    }
    if (!a.lang) {
      return -1;
    } else if (!b.lang) {
      return 1;
    } else if (!a.lang && !b.lang) {
      return 0;
    }
    return compareStrings(a.lang, b.lang);
  });
  const featuresSorted = caps.features.slice();
  featuresSorted.sort();

  let verificationString = "";
  for (const i of identitiesSorted) {
    verificationString +=
      [i.category, i.type, i.lang ?? "", i.name ?? ""]
        .map((val) => val.replace(/</g, "&lt;").replace(/>/g, "&gt;"))
        .join("/") + "<";
  }
  for (const f of featuresSorted) {
    verificationString += f + "<";
  }
  return sha1(verificationString).toString(encBase64);
}

export class CapsModule implements IBaseModule {
  private static BASE_FEATURES = new Set([
    "http://jabber.org/protocol/caps",
    "http://jabber.org/protocol/disco#info",
    "http://jabber.org/protocol/disco#items",
    "urn:xmpp:avatar:metadata+notify",
  ]);
  private _extraFeatures: Set<string> = new Set();
  private _advertisedHash: string;

  constructor(private _identity: ClientIdentity) {
    this._advertisedHash = calculateCapsHash(this._advertisedInfo);
  }

  register(registry: ComponentRegistry): void {
    registry.register(PRESENCE_EXTENSION, this._presenceElement.bind(this));
    registry.register(IQ_HANDLER, {
      namespaceURI: "http://jabber.org/protocol/disco#info",
      handler: this._handleIq.bind(this),
    });
  }

  postRegister(registry: ComponentRegistry): void {
    const extraFeatures = registry.get(ENTITY_FEATURE);
    if (extraFeatures.length) {
      this._extraFeatures = new Set(extraFeatures);
      this._advertisedHash = calculateCapsHash(this._advertisedInfo);
    }
  }

  private get _advertisedInfo(): CapsInfo {
    const featureSet = new Set([
      ...CapsModule.BASE_FEATURES,
      ...this._extraFeatures,
    ]);
    return {
      identities: [
        {
          lang: "en",
          category: "client",
          type: "web",
          name: this._identity.clientName,
        },
      ],
      features: Array.from(featureSet),
    };
  }

  private _presenceElement(): SkylarkJSX.Element {
    return (
      <c
        xmlns="http://jabber.org/protocol/caps"
        hash="sha-1"
        node={this._identity.nodeUri}
        ver={this._advertisedHash}
      />
    );
  }

  private _handleIq(
    _type: "get" | "set",
    payload: Element
  ): SkylarkJSX.Element {
    if (payload.localName === "query") {
      const nodeName = `${this._identity.nodeUri}#${this._advertisedHash}`;
      const identities = this._advertisedInfo.identities.map((i) => (
        <identity
          category={i.category}
          name={i.name}
          type={i.type}
          xml:lang={i.lang}
        />
      ));
      const features = this._advertisedInfo.features.map((f) => (
        <feature var={f} />
      ));
      return (
        <query xmlns="http://jabber.org/protocol/disco#info" node={nodeName}>
          {identities}
          {features}
        </query>
      );
    } else {
      throw new Error("Unknown element");
    }
  }
}
