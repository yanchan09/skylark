import { WebSocketTransport } from "./websocket";

export interface XmppTransport {
  onStanzaReceived?: (stanza: Element) => void;
  onStreamClosed?: () => void;

  openStream(toDomain: string): Promise<void>;
  closeStream(): Promise<void>;

  sendStanza(xmlStanza: string): Promise<void>;
}

export { WebSocketTransport };
