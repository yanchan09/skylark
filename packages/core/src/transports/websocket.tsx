import { XmppTransport } from ".";
import { SkylarkJSX } from "../jsx";

type TransportState =
  | "idle" // No attempt at connecting has been made
  | "connecting" // WebSocket connection has not been established yet
  | "openingStream" // The XMPP stream is being opened
  | "open" // The XMPP stream is open
  | "closing" // The stream has been closed and we are waiting for WebSocket close handshake
  | "closed"; // The connection is closed

export class WebSocketTransport implements XmppTransport {
  private _socketUrl: string;
  private _socket?: WebSocket;
  private _stateBeforeClosing?: TransportState;
  private _state: TransportState = "idle";
  private _domParser = new DOMParser();

  private _pendingStreamOpen?: {
    resolve: () => void;
    reject: (err: any) => void;
  };

  // Event handlers
  onStanzaReceived?: (stanza: Element) => void;
  onStreamClosed?: () => void;

  constructor(url: string) {
    this._socketUrl = url;
  }

  private _doSocketConnect(): Promise<void> {
    this._state = "connecting";
    return new Promise((resolve, reject) => {
      const socket = new WebSocket(this._socketUrl, "xmpp");
      socket.onopen = () => {
        if (socket.protocol !== "xmpp") {
          socket.onerror = null;
          socket.onclose = null;
          socket.close(1003);
          reject(new Error("Server does not support XMPP subprotocol"));
          return;
        }
        socket.onmessage = this._onSocketMessage.bind(this);
        socket.onerror = this._onSocketError.bind(this);
        socket.onclose = this._onSocketClose.bind(this);
        this._socket = socket;
        resolve();
      };
      socket.onerror = () => {
        reject(new Error("Unexpected socket error while connecting"));
      };
      socket.onclose = () => {
        reject(new Error("Unexpected socket close while connecting"));
      };
    });
  }

  private _onSocketMessage(msg: MessageEvent<string>) {
    if (this._state === "closing") {
      // Ignore incoming messages after closing the XMPP stream
      return;
    }

    console.debug(
      "%c<~ %c" + msg.data,
      "font-weight: bold; color: #fca5a5",
      "background-color: #991b1b; color: white"
    );

    const xmlDoc = this._domParser.parseFromString(msg.data, "application/xml");
    const rootElement = xmlDoc.documentElement;
    if (
      rootElement.namespaceURI ===
        "http://www.mozilla.org/newlayout/xml/parsererror.xml" &&
      rootElement.localName === "parsererror"
    ) {
      console.error("XML parse error", rootElement.outerHTML);
      this._closeStreamWithError(
        <not-well-formed xmlns="urn:ietf:params:xml:ns:xmpp-streams" />
      );
    } else if (
      rootElement.namespaceURI === "urn:ietf:params:xml:ns:xmpp-framing"
    ) {
      this._handleStreamHeader(rootElement);
    } else {
      if (this._state !== "open") {
        this._closeStreamWithError(
          <policy-violation xmlns="urn:ietf:params:xml:ns:xmpp-streams" />
        );
      } else {
        this.onStanzaReceived?.(rootElement);
      }
    }
  }

  private _handleStreamHeader(el: Element) {
    if (el.localName === "open") {
      if (this._state === "openingStream") {
        this._state = "open";
        this._pendingStreamOpen?.resolve();
        this._pendingStreamOpen = undefined;
      } else {
        this._closeStreamWithError(
          <policy-violation xmlns="urn:ietf:params:xml:ns:xmpp-streams" />
        );
      }
    } else if (el.localName === "close") {
      this._sendJsxElement(
        <close xmlns="urn:ietf:params:xml:ns:xmpp-framing" />
      );
      this._stateBeforeClosing = this._state;
      this._state = "closing";
    } else {
      this._closeStreamWithError(
        <unsupported-stanza-type xmlns="urn:ietf:params:xml:ns:xmpp-streams" />
      );
    }
  }

  private _onSocketError(_ev: Event) {
    console.error("WebSocket error");
  }

  private _onSocketClose(ev: CloseEvent) {
    console.warn("WebSocket close", ev, this._stateBeforeClosing);
    this._state = "closed";
    if (this._stateBeforeClosing === "openingStream") {
      this._pendingStreamOpen?.reject(
        new Error("The server closed the stream")
      );
      this._pendingStreamOpen = undefined;
    } else if (this._stateBeforeClosing === "open") {
      this.onStreamClosed?.();
    }
    this._stateBeforeClosing = undefined;
    this._socket = undefined;
  }

  private _closeStreamWithError(error: SkylarkJSX.Element) {
    if (!this._socket) {
      throw new Error("No open socket");
    }
    this._sendJsxElement(
      <error xmlns="http://etherx.jabber.org/streams">{error}</error>
    );
    this._sendJsxElement(<close xmlns="urn:ietf:params:xml:ns:xmpp-framing" />);
    this._stateBeforeClosing = this._state;
    this._state = "closing";
    this._socket.close(1000, "An XMPP stream error occurred");
  }

  private _sendJsxElement(el: SkylarkJSX.Element) {
    if (!this._socket) {
      throw new Error("No open socket");
    }
    const xml = SkylarkJSX.toXml(el);
    console.debug(
      "%c~> %c" +
        xml.replace(/(<auth.*>)([A-Za-z0-9=]+)(<\/auth>)/g, "$1..[hidden]..$3"),
      "font-weight: bold; color: #86efac",
      "background-color: #166534; color: white"
    );
    this._socket.send(xml);
  }

  // Interface implementation

  async openStream(toDomain: string): Promise<void> {
    if (
      this._state !== "idle" &&
      this._state !== "closed" &&
      this._state !== "open"
    ) {
      throw new Error("Can't open the stream in this state");
    }
    if (!this._socket) {
      await this._doSocketConnect();
    }
    this._state = "openingStream";
    this._sendJsxElement(
      <open
        xmlns="urn:ietf:params:xml:ns:xmpp-framing"
        to={toDomain}
        version="1.0"
      />
    );
    return new Promise((resolve, reject) => {
      this._pendingStreamOpen = { resolve, reject };
    });
  }

  async closeStream(): Promise<void> {
    if (!this._socket) {
      throw new Error("No open socket");
    }
    this._sendJsxElement(<close xmlns="urn:ietf:params:xml:ns:xmpp-framing" />);
    this._stateBeforeClosing = this._state;
    this._state = "closing";
    this._socket.close(1000, "An XMPP stream error occurred");
  }

  sendStanza(xmlStanza: string): Promise<void> {
    if (this._state !== "open" || !this._socket) {
      throw new Error("Stream not open");
    }
    console.debug(
      "%c~> %c" +
        /*xmlStanza.replace(
          /(<auth.*>)([A-Za-z0-9=]+)(<\/auth>)/g,
          "$1..[hidden]..$3"
        ),*/ xmlStanza,
      "font-weight: bold; color: #86efac",
      "background-color: #166534; color: white"
    );
    this._socket.send(xmlStanza);
    return Promise.resolve();
  }
}
