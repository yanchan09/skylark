import { afterEach, describe, expect, test, vi } from "vitest";
import {
  PlainAuthenticator,
  ScramAuthenticator,
  defaultNonceGenerator,
  saslNegotiate,
} from "./sasl";
import { JID } from "@skylark/jid";

describe("defaultNonceGenerator", () => {
  test("Length is 64", () => {
    expect(defaultNonceGenerator().length).toBe(64);
  });

  test("Generated nonces differ", () => {
    // Sanity check
    const n = defaultNonceGenerator();
    expect(n).toEqual(n);
    // Now the real thing
    for (let i = 0; i < 50; i++) {
      expect(defaultNonceGenerator()).not.toEqual(defaultNonceGenerator());
    }
  });
});

describe("PLAIN", () => {
  const authenticator = new PlainAuthenticator({
    userJid: new JID("alice@xmpp.example.com"),
    password: "password",
  });

  test("initate()", async () => {
    const response = await authenticator.initiate();
    expect(response).toEqual(
      new Uint8Array([
        0, 97, 108, 105, 99, 101, 64, 120, 109, 112, 112, 46, 101, 120, 97, 109,
        112, 108, 101, 46, 99, 111, 109, 0, 112, 97, 115, 115, 119, 111, 114,
        100,
      ])
    );
  });

  test("respond()", async () => {
    await expect(() => authenticator.respond(null)).rejects.toThrowError();
  });

  test("verify()", async () => {
    await authenticator.verify(null);
    await expect(() =>
      authenticator.verify(new Uint8Array([0]))
    ).rejects.toThrowError();
  });
});

describe("SCRAM-SHA-1", () => {
  const authenticator = new ScramAuthenticator(
    {
      userJid: new JID("user@xmpp.example.com"),
      password: "pencil",
    },
    "SHA-1",
    160,
    () => "fyko+d2lbbFgONRv9qkxdawL"
  );

  const CLIENT_INITIATION = new Uint8Array([
    110, 44, 44, 110, 61, 117, 115, 101, 114, 44, 114, 61, 102, 121, 107, 111,
    43, 100, 50, 108, 98, 98, 70, 103, 79, 78, 82, 118, 57, 113, 107, 120, 100,
    97, 119, 76,
  ]);

  const SERVER_RESPONSE = new Uint8Array([
    114, 61, 102, 121, 107, 111, 43, 100, 50, 108, 98, 98, 70, 103, 79, 78, 82,
    118, 57, 113, 107, 120, 100, 97, 119, 76, 51, 114, 102, 99, 78, 72, 89, 74,
    89, 49, 90, 86, 118, 87, 86, 115, 55, 106, 44, 115, 61, 81, 83, 88, 67, 82,
    43, 81, 54, 115, 101, 107, 56, 98, 102, 57, 50, 44, 105, 61, 52, 48, 57, 54,
  ]);

  const SERVER_RESPONSE_WRONG_NONCE = new Uint8Array([
    114, 61, 51, 114, 102, 99, 78, 72, 89, 74, 89, 49, 90, 86, 118, 87, 86, 115,
    55, 106, 51, 114, 102, 99, 78, 72, 89, 74, 89, 49, 90, 86, 118, 87, 86, 115,
    55, 106, 44, 115, 61, 81, 83, 88, 67, 82, 43, 81, 54, 115, 101, 107, 56, 98,
    102, 57, 50, 44, 105, 61, 52, 48, 57, 54,
  ]);

  const CLIENT_RESPONSE = new Uint8Array([
    99, 61, 98, 105, 119, 115, 44, 114, 61, 102, 121, 107, 111, 43, 100, 50,
    108, 98, 98, 70, 103, 79, 78, 82, 118, 57, 113, 107, 120, 100, 97, 119, 76,
    51, 114, 102, 99, 78, 72, 89, 74, 89, 49, 90, 86, 118, 87, 86, 115, 55, 106,
    44, 112, 61, 118, 48, 88, 56, 118, 51, 66, 122, 50, 84, 48, 67, 74, 71, 98,
    74, 81, 121, 70, 48, 88, 43, 72, 73, 52, 84, 115, 61,
  ]);

  const SERVER_VERIFIER = new Uint8Array([
    118, 61, 114, 109, 70, 57, 112, 113, 86, 56, 83, 55, 115, 117, 65, 111, 90,
    87, 106, 97, 52, 100, 74, 82, 107, 70, 115, 75, 81, 61,
  ]);

  test("respond() before initiate() fails", async () => {
    await expect(() =>
      authenticator.respond(SERVER_RESPONSE)
    ).rejects.toThrowError();
  });

  test("initate()", async () => {
    const response = await authenticator.initiate();
    expect(response).toEqual(CLIENT_INITIATION);
  });

  test("verify() before respond() fails", async () => {
    await expect(() =>
      authenticator.verify(SERVER_VERIFIER)
    ).rejects.toThrowError();
  });

  test("respond() with invalid challenge fails", async () => {
    await expect(() => authenticator.respond(null)).rejects.toThrowError();
    await expect(() =>
      authenticator.respond(
        new Uint8Array([
          118, 61, 97, 87, 53, 50, 89, 87, 120, 112, 90, 65, 61, 61,
        ])
      )
    ).rejects.toThrowError();
  });

  test("respond() with mismatching client nonce fails", async () => {
    await expect(() =>
      authenticator.respond(SERVER_RESPONSE_WRONG_NONCE)
    ).rejects.toThrowError();
  });

  test("respond() succeeds", async () => {
    const response = await authenticator.respond(SERVER_RESPONSE);
    expect(response).toEqual(CLIENT_RESPONSE);
  });

  test("verify() fails with invalid response", async () => {
    const INVALID_VERIFIERS = [
      null,
      new Uint8Array([]),
      new Uint8Array([
        118, 61, 97, 87, 53, 50, 89, 87, 120, 112, 90, 65, 61, 61,
      ]),
      new Uint8Array([
        118, 61, 77, 84, 73, 122, 78, 68, 85, 50, 78, 122, 103, 53, 77, 68, 69,
        121, 77, 122, 81, 49, 78, 106, 99, 52, 79, 84, 65, 61,
      ]),
    ];
    for (const vf of INVALID_VERIFIERS) {
      await expect(() => authenticator.verify(vf)).rejects.toThrowError();
    }
  });

  test("verify() succeeds with valid response", async () => {
    await authenticator.verify(SERVER_VERIFIER);
  });
});

describe("Negotiation", () => {
  const testParams = {
    userJid: new JID("alice@xmpp.example.com"),
    password: "password",
  };

  test("SCRAM-SHA-512 is used, when possible", () => {
    const result = saslNegotiate([
      "SCRAM-SHA-1",
      "SCRAM-SHA-256",
      "SCRAM-SHA-512",
      "PLAIN",
      "EXTERNAL",
    ]);
    expect(result.mechanism).toEqual("SCRAM-SHA-512");
    const authenticator = result.factory(testParams);
    expect(authenticator).toBeInstanceOf(ScramAuthenticator);
    expect((<ScramAuthenticator>authenticator).hashParameters).toMatchObject({
      hashType: "SHA-512",
      hashLength: 512,
    });
  });

  test("SCRAM-SHA-256 is used as a fallback", () => {
    const result = saslNegotiate([
      "SCRAM-SHA-1",
      "SCRAM-SHA-256",
      "PLAIN",
      "EXTERNAL",
    ]);
    expect(result.mechanism).toEqual("SCRAM-SHA-256");
    const authenticator = result.factory(testParams);
    expect(authenticator).toBeInstanceOf(ScramAuthenticator);
    expect((<ScramAuthenticator>authenticator).hashParameters).toMatchObject({
      hashType: "SHA-256",
      hashLength: 256,
    });
  });

  test("SCRAM-SHA-1 is used as a fallback", () => {
    const result = saslNegotiate(["SCRAM-SHA-1", "PLAIN", "EXTERNAL"]);
    expect(result.mechanism).toEqual("SCRAM-SHA-1");
    const authenticator = result.factory(testParams);
    expect(authenticator).toBeInstanceOf(ScramAuthenticator);
    expect((<ScramAuthenticator>authenticator).hashParameters).toMatchObject({
      hashType: "SHA-1",
      hashLength: 160,
    });
  });

  test("PLAIN is used as a fallback", () => {
    const result = saslNegotiate(["PLAIN", "EXTERNAL"]);
    expect(result.mechanism).toEqual("PLAIN");
    const authenticator = result.factory(testParams);
    expect(authenticator).toBeInstanceOf(PlainAuthenticator);
  });

  test("Negotiation fails if we can't find any match", () => {
    expect(() => saslNegotiate(["EXTERNAL"])).toThrowError();
  });
});
