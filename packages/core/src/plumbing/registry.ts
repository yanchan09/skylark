import { SkylarkJSX } from "../jsx";

export const MESSAGE_HANDLER = Symbol("MESSAGE_HANDLER") as ComponentKey<
  (elem: Element) => void
>;
export const PRESENCE_EXTENSION = Symbol("PRESENCE_EXTENSION") as ComponentKey<
  () => SkylarkJSX.Element
>;
export const IQ_HANDLER = Symbol(
  "IQ_HANDLER"
) as ComponentKey<RegisteredIqHandler>;

export interface RegisteredIqHandler {
  namespaceURI: string;
  handler: (type: "get" | "set", body: Element) => SkylarkJSX.Element;
}

const valueType = Symbol();

export type ComponentKey<T> = symbol & {
  [valueType]: T;
};

export class ComponentRegistry {
  private componentMap = new Map<any, any>();

  register<T>(key: ComponentKey<T>, value: T) {
    const list = this.componentMap.get(key);
    if (!list) {
      this.componentMap.set(key, [value]);
    } else if (Array.isArray(list)) {
      list.push(value);
    } else {
      throw new Error(`Component ${String(key)} is registered as a singleton`);
    }
  }

  get<T>(key: ComponentKey<T>): T[] {
    const values = this.componentMap.get(key);
    if (values && !Array.isArray(values)) {
      throw new Error(`Component ${String(key)} is registered as a singleton`);
    }
    return values ?? [];
  }
}
