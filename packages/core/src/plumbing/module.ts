import { ComponentRegistry } from "./registry";

export interface IBaseModule {
  register?(registry: ComponentRegistry): void;
  postRegister?(registry: ComponentRegistry): void;
}
