import { acceptHMRUpdate, defineStore } from "pinia";

export interface Message {
  fromJid: string;
  timestamp: Date;
  content: { type: "text"; text: string } | { type: "file"; fileUrl: string };
}

export const useMessagesStore = defineStore("messages", {
  state: () => ({
    conversations: {} as Record<string, Message[]>,
  }),
  getters: {
    forConversation: (state) => {
      return (conversation: string) => state.conversations[conversation] ?? [];
    },
  },
  actions: {
    pushMessage(conversation: string, msg: Message) {
      if (conversation in this.conversations) {
        this.conversations[conversation].push(msg);
        this.conversations[conversation].sort(
          (a, b) => a.timestamp.getDate() - b.timestamp.getDate()
        );
      } else {
        this.conversations[conversation] = [msg];
      }
    },
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useMessagesStore, import.meta.hot));
}
