import { acceptHMRUpdate, defineStore } from "pinia";
import {
  CapsModule,
  Connection,
  PepModule,
  SkylarkJSX,
  WebSocketTransport,
} from "@skylark/core";
import { UserDB } from "../lib/userdb";
import { markRaw } from "vue";
import { useRosterStore } from "./roster";
import { useMessagesStore } from "./messages";
import { JID } from "@skylark/jid";

export enum ConnectionState {
  None = "None",
  Connecting = "Connecting",
  Authenticate = "Authenticate",
  Bind = "Bind",
  SetUpOMEMO = "SetUpOMEMO",
  FetchRoster = "FetchRoster",
  Done = "Done",
  Error = "Error",
}

export interface ConnectParams {
  websocketUrl: string;
  jid: string;
  password: string;
}

export const useConnectionStore = defineStore("connection", {
  state: () => ({
    state: ConnectionState.None,
    connection: null as null | Connection,
    userDb: null as null | UserDB,
    localJid: "",
  }),
  actions: {
    async connect(params: ConnectParams): Promise<void> {
      this.state = ConnectionState.Connecting;
      const transport = new WebSocketTransport(params.websocketUrl);
      const conn = new Connection(
        transport,
        {
          userJid: new JID(params.jid),
          password: params.password,
        },
        [
          new PepModule(),
          new CapsModule({
            nodeUri: "https://codeberg.org/yanchan09/skylark",
            clientName: "Skylark",
          }),
        ]
      );
      this.connection = markRaw(conn);
      window.Skylark = {
        sendPresence(args: any) {
          conn.presence({
            to: args.to ? new JID(args.to) : undefined,
            type: args.type,
          });
        },
      };
      conn.addEventListener("streamOpen", async () => {
        this.state = ConnectionState.Bind;
        const bindResponse = await conn.iq(
          "set",
          <bind xmlns="urn:ietf:params:xml:ns:xmpp-bind" />
        );
        if (!bindResponse) throw new Error("Expected a response to <bind/> iq");
        const assignedJid =
          bindResponse.getElementsByTagNameNS(
            "urn:ietf:params:xml:ns:xmpp-bind",
            "jid"
          )[0].textContent ?? "";
        console.info(`Server bound us to JID ${assignedJid}`);
        this.localJid = assignedJid;

        await conn.iq(
          "get",
          <query xmlns="http://jabber.org/protocol/disco#info" />,
          { to: params.jid.split("@")[1] }
        );

        this.state = ConnectionState.FetchRoster;

        const rosterResponse = await conn.iq(
          "get",
          <query xmlns="jabber:iq:roster" />
        );
        if (!rosterResponse)
          throw new Error("Expected a response to <query/> iq");
        console.log(rosterResponse.outerHTML);
        const rosterStore = useRosterStore();
        rosterStore.entries = Array.from(
          rosterResponse.getElementsByTagNameNS("jabber:iq:roster", "item")
        ).map((el) => {
          return {
            name: el.getAttribute("name") ?? "",
            jid: el.getAttribute("jid") ?? "",
          };
        });
        conn.presence();

        setInterval(() => {
          conn.iq("get", <ping xmlns="urn:xmpp:ping" />);
        }, 5000);

        this.state = ConnectionState.Done;
      });
      conn.addEventListener("message", async (genericEvent: Event) => {
        const event = genericEvent as CustomEvent<Element>;

        if (event.detail.getAttribute("type") !== "chat") return;

        const fromJid = event.detail.getAttribute("from") ?? "";
        const toJid = event.detail.getAttribute("to") ?? "";

        let conversation;
        if (fromJid.split("/")[0] === this.localJid.split("/")[0]) {
          conversation = toJid.split("/")[0];
        } else {
          conversation = fromJid.split("/")[0];
        }
        let content:
          | { type: "text"; text: string }
          | { type: "file"; fileUrl: string };
        const oobElements = event.detail.getElementsByTagNameNS(
          "jabber:x:oob",
          "x"
        );
        if (oobElements.length) {
          content = {
            type: "file",
            fileUrl:
              oobElements[0].getElementsByTagNameNS("jabber:x:oob", "url")[0]
                .textContent ?? "",
          };
        } else {
          const messageBody = event.detail.getElementsByTagNameNS(
            "jabber:client",
            "body"
          )[0];
          content = {
            type: "text",
            text: messageBody.textContent ?? "",
          };
        }
        useMessagesStore().pushMessage(conversation, {
          fromJid: fromJid.split("/")[0],
          content,
          timestamp: new Date(),
        });
      });
    },
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useConnectionStore, import.meta.hot));
}
