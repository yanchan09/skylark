import { acceptHMRUpdate, defineStore } from "pinia";

export interface RosterEntry {
  name: string;
  jid: string;
}

export const useRosterStore = defineStore("roster", {
  state: () => ({
    entries: [] as RosterEntry[],
  }),
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useRosterStore, import.meta.hot));
}
