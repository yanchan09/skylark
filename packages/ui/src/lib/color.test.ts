import { expect, test } from "vitest";
import { hueAngle } from "./color";

test("hueAngle", () => {
  expect(hueAngle("juliet@capulet.lit")).toBeCloseTo(209.4104);
  expect(hueAngle("😺")).toBeCloseTo(331.199341);
});
