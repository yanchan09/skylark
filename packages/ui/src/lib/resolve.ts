export async function resolveWebsocketUrl(domain: string): Promise<string> {
  const metaResponse = await fetch(`https://${domain}/.well-known/host-meta`);
  if (metaResponse.status !== 200) {
    throw new Error(
      `Bad status when fetching host-meta: ${metaResponse.status}`
    );
  }

  const metaText = await metaResponse.text();
  const xmlDocument = new DOMParser().parseFromString(
    metaText,
    "application/xml"
  );

  for (const child of xmlDocument.documentElement.children) {
    if (
      child.localName === "Link" &&
      child.namespaceURI === "http://docs.oasis-open.org/ns/xri/xrd-1.0" &&
      child.getAttribute("rel") === "urn:xmpp:alt-connections:websocket"
    ) {
      const hrefAttr = child.getAttribute("href");
      if (hrefAttr) return hrefAttr;
    }
  }

  throw new Error(
    "host-meta does not include urn:xmpp:alt-connections:websocket link"
  );
}
