// @vitest-environment jsdom
import { setupServer } from "msw/node";
import { rest } from "msw";
import { beforeAll, test, afterAll, expect } from "vitest";
import { resolveWebsocketUrl } from "./resolve";

const server = setupServer(
  rest.get(
    "https://valid.example.com/.well-known/host-meta",
    (_req, res, ctx) => {
      const BODY = `<?xml version='1.0' encoding='utf-8'?>
<XRD xmlns='http://docs.oasis-open.org/ns/xri/xrd-1.0'>
  <Link rel="urn:xmpp:alt-connections:xbosh" href="https://web.example.com:5280/bosh" />
  <Link rel="urn:xmpp:alt-connections:websocket" href="wss://web.example.com:443/ws" />
</XRD>`;
      return res(ctx.status(200), ctx.xml(BODY));
    }
  ),
  rest.get(
    "https://missing.example.com/.well-known/host-meta",
    (_req, res, ctx) => {
      const BODY = `<?xml version='1.0' encoding='utf-8'?>
<XRD xmlns='http://docs.oasis-open.org/ns/xri/xrd-1.0'>
  <Link rel="urn:xmpp:alt-connections:xbosh" href="https://web.example.com:5280/bosh" />
</XRD>`;
      return res(ctx.status(200), ctx.xml(BODY));
    }
  ),
  rest.get(
    "https://invalid.example.com/.well-known/host-meta",
    (_req, res, ctx) => {
      const BODY = `<?xml version='1.0' encoding='utf-8'?>
<XRD xmlns='http://doc`;
      return res(ctx.status(200), ctx.xml(BODY));
    }
  ),
  rest.get(
    "https://notfound.example.com/.well-known/host-meta",
    (_req, res, ctx) => {
      return res(ctx.status(404), ctx.body("Not Found"));
    }
  )
);

beforeAll(() => server.listen({ onUnhandledRequest: "error" }));
afterAll(() => server.close());

test("Valid host-meta", async () => {
  expect(await resolveWebsocketUrl("valid.example.com")).toEqual(
    "wss://web.example.com:443/ws"
  );
});

test("Missing WebSocket URL", async () => {
  await expect(() =>
    resolveWebsocketUrl("missing.example.com")
  ).rejects.toThrowError();
});

test("Invalid XRD", async () => {
  await expect(() =>
    resolveWebsocketUrl("invalid.example.com")
  ).rejects.toThrowError();
});

test("Missing host-meta file", async () => {
  await expect(() =>
    resolveWebsocketUrl("notfound.example.com")
  ).rejects.toThrowError();
});
