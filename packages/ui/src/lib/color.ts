import sha1 from "crypto-js/sha1";
import encHex from "crypto-js/enc-hex";

// Hue angle calculated using the algorithm defined in XEP-0392
// See: https://xmpp.org/extensions/xep-0392.html
export function hueAngle(jid: string): number {
  // Note that we could use Web Crypto API here, but that would be more work + async.
  const hash = sha1(jid).toString(encHex);
  const hashVal = parseInt(hash.slice(2, 4) + hash.slice(0, 2), 16);
  return (hashVal / 65536) * 360;
}
