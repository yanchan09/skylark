import { describe, expect, test } from "vitest";
import { TextBlock, TextSpan, parse, parseToSpans } from "./index";

describe("Span parsing", () => {
  test("Different types of spans", () => {
    const text = [
      "plain text",
      "_emphasised text_",
      "*strongly emphasised text*",
      "~strike through~",
      "`monospace text`",
    ].join(" ");
    expect(parseToSpans(text)).toMatchObject([
      { type: "plain", text: "plain text " },
      {
        type: "em",
        spans: [{ type: "plain", text: "_emphasised text_" }],
      },
      { type: "plain", text: " " },
      {
        type: "strong",
        spans: [{ type: "plain", text: "*strongly emphasised text*" }],
      },
      { type: "plain", text: " " },
      {
        type: "strike",
        spans: [{ type: "plain", text: "~strike through~" }],
      },
      { type: "plain", text: " " },
      { type: "pre", text: "`monospace text`" },
    ] satisfies TextSpan[]);
  });

  test("Different strong spans", () => {
    expect(parseToSpans("*strong*plain*")).toMatchObject([
      {
        type: "strong",
        spans: [{ type: "plain", text: "*strong*" }],
      },
      { type: "plain", text: "plain*" },
    ] satisfies TextSpan[]);
    expect(parseToSpans("*not *strong")).toMatchObject([
      { type: "plain", text: "*not *strong" },
    ] satisfies TextSpan[]);
    expect(parseToSpans("* plain *strong*")).toMatchObject([
      { type: "plain", text: "* plain " },
      {
        type: "strong",
        spans: [{ type: "plain", text: "*strong*" }],
      },
    ] satisfies TextSpan[]);
  });

  test("Strong and preformatted span combinations", () => {
    expect(parseToSpans("This is `*monospace*`")).toMatchObject([
      { type: "plain", text: "This is " },
      { type: "pre", text: "`*monospace*`" },
    ] satisfies TextSpan[]);
    expect(parseToSpans("This is *`monospace and bold`*")).toMatchObject([
      { type: "plain", text: "This is " },
      {
        type: "strong",
        spans: [
          { type: "plain", text: "*" },
          { type: "pre", text: "`monospace and bold`" },
          { type: "plain", text: "*" },
        ],
      },
    ] satisfies TextSpan[]);
  });

  test("Empty spans are ignored", () => {
    expect(parseToSpans("**")).toMatchObject([
      { type: "plain", text: "**" },
    ] satisfies TextSpan[]);
    expect(parseToSpans("***")).toMatchObject([
      { type: "plain", text: "***" },
    ] satisfies TextSpan[]);
  });
});

describe("Block parsing", () => {
  test("Preformatted blocks", () => {
    const text = `\`\`\`
print("Hello world!")
\`\`\`
Look at this cool code I wrote!`;
    expect(parse(text)).toMatchObject([
      {
        type: "pre",
        content: 'print("Hello world!")',
      },
      {
        type: "plain",
        spans: [
          {
            type: "plain",
            text: "Look at this cool code I wrote!",
          },
        ],
      },
    ] satisfies TextBlock[]);
  });

  test("Preformatted blocks inside quote blocks", () => {
    const text = `> \`\`\`
> print("Hello world!")
> \`\`\`
Look at this cool code I wrote!`;
    expect(parse(text)).toMatchObject([
      {
        type: "quote",
        children: [
          {
            type: "pre",
            content: 'print("Hello world!")',
          },
        ],
      },
      {
        type: "plain",
        spans: [
          {
            type: "plain",
            text: "Look at this cool code I wrote!",
          },
        ],
      },
    ] satisfies TextBlock[]);
  });

  test("Text after closing and opening grave accents", () => {
    // Note that Gajim for example parses this differently. Reading XEP-0393,
    // behaviour as described here seems to be more correct, though.
    const text = `\`\`\`ignored
print("Hello world!")
\`\`\`oops
print("Oh no!")`;
    expect(parse(text)).toMatchObject([
      {
        type: "pre",
        content: 'print("Hello world!")\n```oops\nprint("Oh no!")',
      },
    ] satisfies TextBlock[]);
  });
});

describe("/me command", () => {
  test("At the start of the message", () => {
    const text = "/me is feeling excited";
    expect(parse(text)).toMatchObject([
      {
        type: "me",
        content: "is feeling excited",
      },
    ] satisfies TextBlock[]);
  });

  // Note that this *intentionally* diverges from the behaviour recommended in XEP-0245.
  // It's common for people to send /me command on second line of the message (not first).
  // As such, we should also handle that case.
  test("On another line", () => {
    const text = "Good morning!\n/me is feeling excited";
    expect(parse(text)).toMatchObject([
      {
        type: "plain",
        spans: [{ type: "plain", text: "Good morning!" }],
      },
      {
        type: "me",
        content: "is feeling excited",
      },
    ] satisfies TextBlock[]);
  });

  test("Not parsed inside a block", () => {
    const text = "> /me is feeling excited\nOoh, why?";
    expect(parse(text)).toMatchObject([
      {
        type: "quote",
        children: [
          {
            type: "plain",
            spans: [{ type: "plain", text: "/me is feeling excited" }],
          },
        ],
      },
      {
        type: "plain",
        spans: [{ type: "plain", text: "Ooh, why?" }],
      },
    ] satisfies TextBlock[]);
  });
});
