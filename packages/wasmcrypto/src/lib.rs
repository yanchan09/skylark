mod utils;

use std::convert::TryInto;

use curve25519_dalek::edwards::CompressedEdwardsY;
use ed25519_dalek::{ExpandedSecretKey, Signer};
use rand::rngs::OsRng;
use thiserror::Error;
use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[derive(Error, Debug)]
pub enum Error {
    #[error("invalid pubkey size")]
    InvalidPubkeySize,
    #[error("invalid serialized keypair size")]
    InvalidSerializedKeypairSize,
    #[error("invalid Ed25519 public key")]
    InvalidEd25519PublicKey,
    #[error("invalid signature")]
    InvalidSignature,
}

impl Into<JsValue> for Error {
    fn into(self) -> JsValue {
        JsValue::from_str(&format!("[wasmcrypto] {}", self))
    }
}

type Result<T> = std::result::Result<T, Error>;

#[wasm_bindgen]
#[derive(Clone, Copy, Debug)]
pub enum PubkeyKind {
    X25519,
    Ed25519,
}

#[wasm_bindgen]
pub fn initialize() {
    utils::set_panic_hook();
}

fn x25519_pubkey_from_any(kind: PubkeyKind, pubkey: &[u8]) -> Result<x25519_dalek::PublicKey> {
    let key_array: [u8; 32] = pubkey.try_into().map_err(|_| Error::InvalidPubkeySize)?;

    let converted_pubkey = match kind {
        PubkeyKind::X25519 => key_array,
        PubkeyKind::Ed25519 => CompressedEdwardsY(key_array)
            .decompress()
            .ok_or(Error::InvalidEd25519PublicKey)?
            .to_montgomery()
            .to_bytes(),
    };

    Ok(x25519_dalek::PublicKey::from(converted_pubkey))
}

#[wasm_bindgen]
pub struct Ed25519KeyPair(ed25519_dalek::Keypair);

#[wasm_bindgen]
impl Ed25519KeyPair {
    pub fn generate() -> Self {
        Self(ed25519_dalek::Keypair::generate(&mut OsRng))
    }

    pub fn deserialize(data: &[u8]) -> Result<Ed25519KeyPair> {
        let keypair = ed25519_dalek::Keypair::from_bytes(data)
            .map_err(|_| Error::InvalidSerializedKeypairSize)?;
        Ok(Self(keypair))
    }

    pub fn public_bytes(&self) -> Box<[u8]> {
        Box::new(self.0.public.to_bytes())
    }

    pub fn serialize(&self) -> Box<[u8]> {
        Box::new(self.0.to_bytes())
    }

    pub fn dh(&self, their_pubkey_kind: PubkeyKind, their_pubkey: &[u8]) -> Result<Box<[u8]>> {
        let expanded_key = ExpandedSecretKey::from(&self.0.secret);
        let mut secret = [0u8; 32];
        secret.copy_from_slice(&expanded_key.to_bytes()[0..32]);
        let x25519_sk = x25519_dalek::StaticSecret::from(secret);

        let pubkey = x25519_pubkey_from_any(their_pubkey_kind, their_pubkey)?;
        let shared_secret = x25519_sk.diffie_hellman(&pubkey).to_bytes();
        Ok(Box::new(shared_secret))
    }

    pub fn sign(&self, message: &[u8]) -> Box<[u8]> {
        Box::new(self.0.sign(message).to_bytes())
    }
}

#[wasm_bindgen]
pub fn verify_ed25519_signature(pubkey: &[u8], message: &[u8], signature: &[u8]) -> Result<()> {
    let pubkey =
        ed25519_dalek::PublicKey::from_bytes(pubkey).map_err(|_| Error::InvalidEd25519PublicKey)?;
    let signature =
        ed25519_dalek::Signature::from_bytes(signature).map_err(|_| Error::InvalidSignature)?;

    pubkey
        .verify_strict(message, &signature)
        .map_err(|_| Error::InvalidSignature)?;

    Ok(())
}

#[wasm_bindgen]
pub struct X25519KeyPair(x25519_dalek::StaticSecret);

#[wasm_bindgen]
impl X25519KeyPair {
    pub fn generate() -> Self {
        Self(x25519_dalek::StaticSecret::new(&mut OsRng))
    }

    pub fn deserialize(data: &[u8]) -> Result<X25519KeyPair> {
        let data_array: [u8; 32] = data
            .try_into()
            .map_err(|_| Error::InvalidSerializedKeypairSize)?;
        let secret = x25519_dalek::StaticSecret::from(data_array);
        Ok(Self(secret))
    }

    pub fn public_bytes(&self) -> Box<[u8]> {
        let pubkey = x25519_dalek::PublicKey::from(&self.0);
        Box::new(pubkey.to_bytes())
    }

    pub fn serialize(&self) -> Box<[u8]> {
        Box::new(self.0.to_bytes())
    }

    pub fn clone(&self) -> Self {
        Self(self.0.clone())
    }

    pub fn dh(&self, their_pubkey_kind: PubkeyKind, their_pubkey: &[u8]) -> Result<Box<[u8]>> {
        let pubkey = x25519_pubkey_from_any(their_pubkey_kind, their_pubkey)?;
        let shared_secret = self.0.diffie_hellman(&pubkey).to_bytes();
        Ok(Box::new(shared_secret))
    }
}
